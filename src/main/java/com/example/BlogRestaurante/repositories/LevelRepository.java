package com.example.BlogRestaurante.repositories;

import com.example.BlogRestaurante.entities.City;
import com.example.BlogRestaurante.entities.Level;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

/**
 * Created by alexcuiza on 9/1/18.
 */

@Transactional
public interface LevelRepository  extends JpaRepository<Level,Integer> {

    Level findById(Integer id);


}
