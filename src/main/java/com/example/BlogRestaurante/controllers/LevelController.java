package com.example.BlogRestaurante.controllers;

import com.example.BlogRestaurante.entities.Choice;
import com.example.BlogRestaurante.entities.Level;
import com.example.BlogRestaurante.services.ChoiceService;
import com.example.BlogRestaurante.services.LevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by alexcuiza on 9/1/18.
 */

@Controller
public class LevelController {

    @Autowired
    private LevelService levelService;

    @RequestMapping(value="/leves",method = RequestMethod.GET)
    public String listas (Model model)
    {

        model.addAttribute("levels",levelService.listAllLeves());

        return "leves";
    }


    @RequestMapping(value = "/level/{id}", method = RequestMethod.GET)
    public String showLevel(@PathVariable Integer id, Model model) {
        model.addAttribute("level",levelService.getLevelById(id));
        return "mostrarLevels";
    }

    @RequestMapping(value = "/level/new", method = RequestMethod.GET)
    public String newChoice(Model model) {
        model.addAttribute("level", new Level());

        return "newLevel";
    }

    @RequestMapping(value = "/level", method = RequestMethod.POST)
    public String save(@Valid Level level, BindingResult bindingResult, Model model) {


        if (bindingResult.hasErrors()) {
            return "newLevel";
        }
        levelService.saveLevel(level);
        return "redirect:/leves";
    }

    @RequestMapping(value = "/level/edit/{id}", method = RequestMethod.GET)
    public String editPost(@PathVariable Integer id, Model model) {
        model.addAttribute("level", levelService.getLevelById(id));
        return "newLevel";
    }
    @RequestMapping(value = "/level/delete/{id}", method = RequestMethod.GET)
    public String deletePost(@PathVariable Integer id, Model model) {
        levelService.deleteLevel(id);
        return "redirect:/leves";
    }


}
