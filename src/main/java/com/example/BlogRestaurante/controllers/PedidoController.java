package com.example.BlogRestaurante.controllers;

import com.example.BlogRestaurante.entities.Choice;
import com.example.BlogRestaurante.entities.Pedido;

import com.example.BlogRestaurante.entities.Restaurant;
import com.example.BlogRestaurante.entities.User;
import com.example.BlogRestaurante.services.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.sql.Types.NULL;


@Controller
public class PedidoController {

    @Autowired
    private PedidoService pedidoService;


    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private ChoiceService choiceService;


    @Autowired
    private UserService userService;


    @Autowired
    private EstadoService estadoService;


    @RequestMapping(value="/pedidos",method = RequestMethod.GET)
    public String listas (Model model)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());


        if(user.isIdentificadorOpcion() == false){
            model.addAttribute("pedido", new Pedido());
            model.addAttribute("restaurants", restaurantService.listAllRestaurants());
            model.addAttribute("choices", choiceService.listAllOptions());
            //model.addAttribute("users",userService.listAllUsers());
            model.addAttribute("users",user); //cargando ID

            return "newPedido";
        }

        model.addAttribute("pedidos", pedidoService.listAllOptions());
        //model.addAttribute("users",userService.listAllUsers());
        model.addAttribute("users",userService.getUserById(user.getId()));
        model.addAttribute("users",user);  //Cargando ID

        Set<String> roles = AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities());

        if (roles.contains("ADMINREST")) {
            return "redirect:/adminrest/home";
        }

        return "redirect:/user/pedidos";
        //return "redirect:/user/pedidos"; original
    }


    @RequestMapping(value = "/pedido/new", method = RequestMethod.GET)
    public String newPedido(Model model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        //si el identificador opcion es  true significa que ya no es la primera vez que hizo un pedido


        if(user.isIdentificadorOpcion()== true){


            return "redirect:/pedido/newTwo";

        }


        List<Restaurant> listRestaurants = new ArrayList<>();
        Iterable<Restaurant> listaOriginal = restaurantService.listAllRestaurants();


        for (Restaurant r : listaOriginal ){
            if(r.getId_city_rest().getId() == user.getId_city().getId())
            {
                listRestaurants.add(r);
            }
        }
        //se crea un objeto new pedido
        model.addAttribute("pedido", new Pedido());
        model.addAttribute("restaurants", listRestaurants);
        model.addAttribute("choices", choiceService.listAllOptions());
        model.addAttribute("users",user);

        return "newPedido";
    }



    @RequestMapping(value ="/pedido/newTwo",method = RequestMethod.GET)
    public String newPedidoTwo(Model model){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        Iterable<Pedido> pedidos  = pedidoService.listAllOptions();
        ArrayList<Pedido> listaDePedidosUsuario = new ArrayList<>();


        for (Pedido pedido:  pedidos) {

            if(pedido.getId_user().getId() == user.getId())
            {

                listaDePedidosUsuario.add(pedido);
            }


        }

        Pedido pedido =listaDePedidosUsuario.get(listaDePedidosUsuario.size()-1);


        List<Restaurant> listRestaurants = new ArrayList<>();
        Iterable<Restaurant> listaOriginal = restaurantService.listAllRestaurants();


        for (Restaurant r : listaOriginal ){
            if(r.getId_city_rest().getId() == user.getId_city().getId())
            {
                listRestaurants.add(r);
            }
        }


        Pedido nuevoPedido = new Pedido();
        nuevoPedido.setCategory_restaurant(pedido.getCategory_restaurant());

        model.addAttribute("pedido",nuevoPedido);
        model.addAttribute("pedidoDefect",pedido);
        model.addAttribute("restaurants",listRestaurants);
        model.addAttribute("choices", choiceService.listAllOptions());
        model.addAttribute("users",user);

        return "newPedidoTwo";


    }

    @RequestMapping(value = "/nextPedido", method = RequestMethod.POST)
    public String nextPedido(@Valid Pedido pedido, BindingResult bindingResult, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        Iterable<Choice> allChices = choiceService.listAllOptions();
        ArrayList<Choice> listChoices = new ArrayList<>();

        for (Choice c : allChices) {
            if (c.getCategory_restaurant().getId() == pedido.getCategory_restaurant().getId()) {
                listChoices.add(c);
            }
        }

        model.addAttribute("pedido", pedido);
        model.addAttribute("choices", listChoices);
        model.addAttribute("users", user);
        model.addAttribute("estados", estadoService.getEstadoById(1));

        return "nextPedido";
    }

    @RequestMapping(value = "/nextPedidoTwo", method = RequestMethod.POST)
    public String nextPedidoTwo(@Valid Pedido pedido, BindingResult bindingResult, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());



        Iterable<Pedido> pedidos  = pedidoService.listAllOptions();
        ArrayList<Pedido> listaDePedidosUsuario = new ArrayList<>();


        for (Pedido pedidoc:  pedidos) {

            if(pedidoc.getId_user().getId() == user.getId())
            {

                listaDePedidosUsuario.add(pedidoc);
            }


        }

        Pedido pedidoUltimo =listaDePedidosUsuario.get(listaDePedidosUsuario.size()-1);


        Iterable<Choice> allChices = choiceService.listAllOptions();
        ArrayList<Choice> listChoices = new ArrayList<>();

        for (Choice c : allChices) {
            if (c.getCategory_restaurant().getId() == pedido.getCategory_restaurant().getId()) {
                listChoices.add(c);
            }
        }

        pedido.setLongitudPedido(pedidoUltimo.getLongitudPedido());
        pedido.setLatitudPedido(pedidoUltimo.getLatitudPedido());
        //cambio para que sepa que opcion es de la ultima pedido que hizo
        pedido.setCategory_choice(pedidoUltimo.getCategory_choice());
        pedido.setDireccion(pedidoUltimo.getDireccion());



        model.addAttribute("pedido", pedido);
        model.addAttribute("choices", listChoices);
        model.addAttribute("users", user);
        model.addAttribute("estados", estadoService.getEstadoById(1));

        return "nextPedido";
    }


    @RequestMapping(value = "/pedido", method = RequestMethod.POST)
    public String save(@Valid Pedido pedido, BindingResult bindingResult, Model model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        Iterable<Pedido> pedidos = pedidoService.listAllOptions();

        user.setIdentificadorOpcion(true);

        pedido.setId_user(user);
        pedidoService.saveRestaurant(pedido);
        userService.saveUserEdited(user);

        //model.addAttribute("user",user);

        Set<String> roles = AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities());

        /*
        if (roles.contains("ADMINREST")) {



            return "redirect:/adminrest/home";
        }
        */

        return "redirect:/map";




        //return "redirect:/user/home";

    }

    @RequestMapping(value = "/map", method = RequestMethod.GET)
    public String showMap( Model model){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        //filtrar los pedidos de los usuarios

        Iterable<Pedido> pedidos  = pedidoService.listAllOptions();
        ArrayList<Pedido> listaDePedidosUsuario = new ArrayList<>();


        for (Pedido pedido:  pedidos) {

            if(pedido.getId_user().getId() == user.getId())
            {


                listaDePedidosUsuario.add(pedido);
            }


        }

        Pedido ultimoPedido =listaDePedidosUsuario.get(listaDePedidosUsuario.size()-1);
        Restaurant restaurantQuePidio = restaurantService.getRestaurantById(ultimoPedido.getCategory_restaurant().getId());

        model.addAttribute("pedido",ultimoPedido);
        model.addAttribute("restaurant",restaurantQuePidio);

        return "map";


    }

    @RequestMapping(value = "/pedidoState",method = RequestMethod.POST)
    public String saveStateChoice(@Valid Pedido pedido, BindingResult bindingResult, Model model){

        Set<String> roles = AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
      //  if (roles.contains("ADMINREST")) {


           Pedido pedidoUsuario  =pedidoService.getRestaurantById(pedido.getId());

            pedido.setId_user(pedidoUsuario.getId_user());
            pedidoService.saveRestaurant(pedido);

            return "redirect:/adminrest/home";



    }



    @RequestMapping(value = "/user/pedidos", method = RequestMethod.GET)
    public String listOfPedidos( Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        Iterable<Pedido> allPedidos = pedidoService.listAllOptions();
        ArrayList<Pedido> listPedidos = new ArrayList<>();

        /*
        for (Pedido p : allPedidos) {
            if (p.getCategory_restaurant().getUser().getId() == user.getId()) {
                listPedidos.add(p);
            }
        }
        */

        //filtramos los pedidos del usuario
        for (Pedido pedido: allPedidos) {
            if(pedido.getId_user().getId()== user.getId()){

                listPedidos.add(pedido);
            }

        }

        //cambio

        model.addAttribute("pedidos", listPedidos);
        model.addAttribute("user",user);
        //model.addAttribute("pedido", new Pedido(user));
        //model.addAttribute("users", userService.listAllUsers());
        //return "listsPedidos";
        return "optionsPedidosUser"; //Corrigiendo error de mostrarpedidos




        //return "optionsPedidosUser"; //probando con otro html para user
    }

    @RequestMapping(value = "/pedido/edit/{id}", method = RequestMethod.GET)
    public String editPedido(@PathVariable Integer id, Model model) {

        Pedido pedido = pedidoService.getRestaurantById(id);

        model.addAttribute("user",userService.getUserById(pedido.getId_user().getId()));
        model.addAttribute("pedido", pedidoService.getRestaurantById(id));
        model.addAttribute("estados",estadoService.listAllEstados());

        Set<String> roles = AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities());

        if (roles.contains("ADMINREST")) {

            return "editEstadoPedido";
        }


        model.addAttribute("estadoss", estadoService.getEstadoById(pedido.getEstadopedido().getId()));

        return "editDireccion";



        //return "editDireccion";

    }

    /** ELMINA UN REGISTRO DE UN [PEDIDO]**/
    @RequestMapping(value = "/pedido/delete/{id}", method = RequestMethod.GET)
    public String deleteRestaurante(@PathVariable Integer id){
        pedidoService.deletePedido(id);
        return "redirect:/pedidos";
    }



    /** MUESTRA EL DETALLE DE UN PEDIDO**/
    @Autowired
    public void setPedidoService(PedidoService pedidoService) {
        this.pedidoService = pedidoService;
    }

//    /** MUESTRA EL DETALLE DE UN RESTAURANTE **/
//    @Autowired
//    public void setRestaurantService(RestaurantService restaurantService) {
//        this.restaurantService = restaurantService;
//    }

    @RequestMapping(value = "/pedido/{id}", method = RequestMethod.GET)
    public String showPedido(@PathVariable Integer id, Model model) {
        model.addAttribute("pedido", pedidoService.getRestaurantById(id));
        model.addAttribute("choice", choiceService.getRestaurantById(id));


        Set<String> roles = AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities());

        if (roles.contains("ADMINREST")) {
            return "mostrarPedidoAdmRest";
        }
        return "mostrarPedido";


        //return "mostrarPedido";//original
    }

    @RequestMapping(value = "/pedido/map/{id}", method = RequestMethod.GET)
    public String showMapPedido(@PathVariable Integer id,Model model){




        Pedido pedidoUsuario = pedidoService.getRestaurantById(id);
        Restaurant restaurantQuePidio = restaurantService.getRestaurantById(pedidoUsuario.getCategory_restaurant().getId());

        model.addAttribute("pedido",pedidoUsuario);
        model.addAttribute("restaurant",restaurantQuePidio);

        Set<String> roles = AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
        if (roles.contains("ADMINREST")) {
            return "mapAdmRest";
        }
        return "map";


    }


}
