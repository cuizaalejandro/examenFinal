package com.example.BlogRestaurante.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by alexcuiza on 9/1/18.
 */
@Entity
@Table(name = "level")
public class Level {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "nombreLevel")
    @NotNull
    @Size(min = 1,max = 50,message = "Debe ser mayor a 1 y menor a 50 letras")
    private String nombre_level;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre_level() {
        return nombre_level;
    }

    public void setNombre_level(String nombre_level) {
        this.nombre_level = nombre_level;
    }
}
