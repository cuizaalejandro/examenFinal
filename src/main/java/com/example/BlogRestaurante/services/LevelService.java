package com.example.BlogRestaurante.services;

import com.example.BlogRestaurante.entities.City;
import com.example.BlogRestaurante.entities.Level;

/**
 * Created by alexcuiza on 9/1/18.
 */
public interface LevelService {

    Iterable<Level> listAllLeves();
    Level getLevelById(Integer id);
    Level saveLevel(Level level);
    void deleteLevel(Integer id);
}
