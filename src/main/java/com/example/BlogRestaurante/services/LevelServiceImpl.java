package com.example.BlogRestaurante.services;

import com.example.BlogRestaurante.entities.Level;
import com.example.BlogRestaurante.repositories.CityRepository;
import com.example.BlogRestaurante.repositories.LevelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by alexcuiza on 9/1/18.
 */
@Service
public class LevelServiceImpl  implements  LevelService  {


    @Autowired
    LevelRepository levelRepository;
    @Override
    public Iterable<Level> listAllLeves() {
        return levelRepository.findAll();
    }

    @Override
    public Level getLevelById(Integer id) {
        return levelRepository.findById(id);
    }

    @Override
    public Level saveLevel(Level level) {
        return levelRepository.save(level);
    }

    @Override
    public void deleteLevel(Integer id) {

        levelRepository.delete(id);

    }
}
