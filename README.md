# Taller De Programacion 2-2017 Universidad Catolica Boliviana "San Pablo"


PROYECTO ADMINISTRADOR DE PEDIDOS DE RESTAURANTES
=================================================

Descripcion del proyecto
------------------------
- El proyecto basicamente realiza pedidos a los restaurantes registrados.
- Permite registrar usuarios para luego realizar pedidos.
- Permite crear administradores para cada restaurante para administrar los pedidos.
- Permite identificar la direccion de los pedidos mediante google maps.



Integrantes
-----------
- José Luis Anagua Quispe
- Alvaro Cuiza Laime
- Robert Camacho Lara
- Estanislao Vargas Vargas



Programas Empleados
-------------------
- IntelliJ IDEA 2017.2.3
- XAMPP Control Panel v3.2.2
- HeidiSQL 9.4.0.5125 
- http://www.bufa.es/google-maps-latitud-longitud/ "Para Calcular longitud y latitud manualmente".


Forma de crear la Base de Datos
-------------------------------
en la tabla **role** se crearan las siguientes filas:

role_id|role 
----|---- 
1|ADMIN
2|LIMITED
3|ADMINREST
 
en la tabla **estado** se creara las siguientes filas

id|estadope 
----|---- 
 1|PENDIENTE 
 2|ENVIADO 
 3|ENTREGADO
  

Administrador, Usuarios y Administradores de restaurantes
=========================================================

Administrador General
---------------------
email|contraseña
----|----
stanis@gmail.com 123456

Administrador Restaurante 1
---------------------------
email|contraseña
----|----
robert@gmail.com 123456


Administrador Restaurante 2
---------------------------
email|contraseña
----|----
daniel@gmail.com 123456


Usuarios
--------
email|contraseña
----|----
alvaro@gmail.com | 123456
joseluis@gmail.com | 123456


